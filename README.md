This is a template repository for the `grid` package to be developed in a student project within the
course "Sustainable Development of Simulation Software". For a full description of the project, see
[here][projectreadme]. This template repository predefines a folder structure for the project as well as
a basic [CI](https://docs.gitlab.com/ee/ci/) that automatically executes the tests, lints the code with
[flake8](https://flake8.pycqa.org/en/latest/) and does type checking with [mypy](https://mypy-lang.org/).
To execute these tasks in the command line, move to the top folder of this repository and type:

```bash
python -m pytest
flake8
mypy grid
```

__Note__: Replace this `README` with an actual description of the project during development.


Requirements
============

As outlined in the [project description][projectreadme], we want to perform computations on grids of different
dimension and composed of different geometric primitives. Independent of the type of grid, we need to be able to

- query the number of points of a grid
- query the number of cells of a grid
- iterate over the points of the grid (only required for VTK output, see below)
- iterate over the grid cells and have the possibility to
    - get a unique index for the cell
    - get its volume
    - get its center point
    - iterate over the corner points of a cell (only required for VTK output, see below) and
       - get a unique index per point (to determine the cell connectivity)
    - iterate over its faces and
        - get its normal vector
        - get its center
        - get its area
        - query if the face is on the boundary
        - get the outside cell (if not on the boundary)

It is suggested to start with the implementation of a structured, rectangular grid in 2d (or even 1d), for which
the geometric properties and connectivity information are trivial to compute. The implementation of general, unstructured
grids should be started once the [geometry](https://gitlab.com/sustainable-simulation-software/group-work-templates/geometry)
package is available.

Moreover, we would like to visualize the grid using [ParaView](https://www.paraview.org/). To this end, functionality
to export a grid into a [VTK file format](https://kitware.github.io/vtk-examples/site/VTKFileFormats/) should be added. It
is recommended to use the [VTU file format](https://kitware.github.io/vtk-examples/site/VTKFileFormats/#unstructuredgrid),
which is able to represent generic grids (__Note__: depending on the size of the group and the project progress, this functionality may be added by the project tutors). A minimum example of such a file for a grid with a single quadrilateral cell looks
like this:

```xml
<?xml version="1.0"?>
<VTKFile type="UnstructuredGrid" version="0.1">
  <UnstructuredGrid>
    <Piece NumberOfPoints="4" NumberOfCells="1">
    <PointData>
      <DataArray Name="point_data" type="Float64" NumberOfComponents="1" format="ascii">
        0 1 2 3
      </DataArray>
    </PointData>
    <CellData>
      <DataArray Name="cell_data" type="Float64" NumberOfComponents="1" format="ascii">
        2.2
      </DataArray>
    </CellData>
    <Points>
        <DataArray Name="Points" type="Float64" NumberOfComponents="3" format="ascii">
          0.0 0.0 0.0 1.0 0.0 0.0 1.0 1.0 0.0 0.0 1.0 0.0
        </DataArray>
    </Points>
    <Cells>
      <DataArray Name="connectivity" type="Float64" NumberOfComponents="1" format="ascii">
        0 1 2 3
      </DataArray>
      <DataArray Name="offsets" type="Float64" NumberOfComponents="1" format="ascii">
        4
      </DataArray>
      <DataArray Name="types" type="Float64" NumberOfComponents="1" format="ascii">
        9
      </DataArray>
    </Cells>
    </Piece>
  </UnstructuredGrid>
</VTKFile>
```


Development suggestions
=======================

It is recommended to follow a [TDD](https://en.wikipedia.org/wiki/Test-driven_development)-like development approach.
That is, start with thinking about the next (minimal) development step, and write a test for it __before__ starting the
actual development. This way, you immediately see what the usage of your code looks like, which can help you to design
a user-friendly API. As an example, you may want to start the project with implementing a `StructuredGrid` class. The first
thing you will need is a way to construct such a class, so you may start by defining a test for it:

```python
from grid import StructuredGrid

def test_structured_grid_2d_construction():
    grid = StructuredGrid(
        size=(1.0, 1.0),
        resolution=(10, 10),
        # more parameters needed?
    )
```

This test fails because the implementation is missing. Thus, the next task is to provide an implementation
that makes the test pass.

It is helpful to always only implement the minimum amount of code required to make a test pass. For instance,
let's say we want to implement a way to iterate over the cells of a grid, and we write a test like this:

```python
def test_structured_grid_cell_iterator():
    grid = ...
    number_of_cells = ...
    # count the number of cells the cell iterator yields and make sure the number matches
    assert sum(1 for _ in grid.cells) == number_of_cells
```

The above test assumes that `grid.cells` returns something that can be iterated over and that it yields exactly
`number_of_cells` elements. A minimum implementation that makes this test pass could look like this:

```python
class StructuredGrid:
    # ...
    @property
    def cells(self) -> Iterable:
        # At this point, let's not worry about what type a cell is and what
        # functionality it should expose. Let us simply return something that
        # we can iterate over and which has a size equal to the number of cells.
        # Let's return a generator expression (https://www.pythontutorial.net/advanced-python/python-generator-expressions/)
        return (i for i in range(self._number_of_cells))

        # Alternatively, you could also directly return the range...
        # return range(self._number_of_cells)

        # But the generator syntax lets you substitute the type of elements in a subsequent step.
        # Let's say we introduce an empty Cell class as a next step towards iterating over cells:
        # class Cell:
        #     ...
        # We can then return a generator expression over cells:
        # return (Cell() for _ in range(self._number_of_cells))

        # afterwards, we could then enrich the Cell class step by step to fulfill our requirements...
```

This makes the above test pass. But of course, now we iterate over integers which do not represent grid cells.
Thus, once we move to the next test, in which we want to check that a cell provides a specific property that we
require (e.g. its volume), we will have to enhance the implementation of the `cells` function. Take a look at the
comments in the above code snippet (below the `return` statement) for hints on how you could proceed.

[projectreadme]: https://gitlab.com/sustainable-simulation-software/course-material/-/tree/main/project
